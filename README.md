Time Barred Token
=================

Information
-----------
* Author: Mihail Sablin <mihail@webprogrammist.com>
* Licence: http://www.opensource.org/licenses/mit-license.php MIT licence
* Project page: https://bitbucket.org/migelsabre/timebarredtoken/
* Git repository: https://migelsabre@bitbucket.org/migelsabre/timebarredtoken.git


Description
-----------
Small extension for [Yii framework](http://http://www.yiiframework.com/), providing an API for creating and validating an encrypted token that has a limited lifetime,
and also provides a [filter](http://www.yiiframework.com/doc/api/1.1/CController#filters-detail) to allow or deny execution of controller actions, depending on the validity of the token.

Installation
------------
1.  Put files into protected/extension/TimeBarredToken folder
2.  Add TimeBarredTokenComponent to your application components in protected/config/main.php:

		'components' => array(
			....
			'timeBarredToken' => array(
				'class' => 'ext.TimeBarredToken.TimeBarredTokenComponent',
				/* This is optional: */
				'duration' => 3600,
				'encryptionKey' => 'mmn!$89MmdiopNWuIOOEWR-0AA689',
				'validationKey' => '903*(E)0k909eijj1@#0-',
			),
			....

		),

Usage
-----
Here is example controller, which allow access to http://<youdomain.com>/article/details from 15:00 till 16:00 only for user, that have right token:

	class ArticleController extends CController {
		public function filters() {
			return array(
				'ext.TimeBarredToken.TimeBarredTokenFilter + details',
				'throwException' => true,
				'message' => 'Access to this article denied!',
			);
		}

		/* This action shows you protected page */
		public function actionDetails() {
			$this->render('details');
		}

		/* This action generates link with token and should be accessible only for authorised users - usually only for you */
		public function actionMySecretLinkGenerator() {
			echo Yii::app()->createUrl('article/details', array('token' => Yii::app()->timeBarredToken->getToken(strtotime('21 december 2011 15:00'), 3600)));
		}
	}


Now, you can create a marker with page http://<youdomain.com>/article/mySecretLinkGenerator and the web page http://<youdomain.com>/article/details can be achieved using a link like this:

http://<youdomain.com>/article/details/?token=ZDhmOWUwODZjZGIxYWQzNmFlNWQzMDljZDRhODk4MDhmZGViNGYzY0-VRvptB2PtTp21p1SHohyGOxenhDj_INl54m8XiuHT3KLx2WQaFKnw38mywzpm23XT8M_dwgkGWkvGRw4i8ODwYbOyS-t02Ygt5cWJAvSu

Access will be limited from 15:00 till 16:00 21 dec 2012

Important note
-----
Token contains only time limits, not also route, so many pages may be accessible with one valid token!
