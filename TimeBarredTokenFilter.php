<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2012, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 * @link https://bitbucket.org/migelsabre/timebarredtoken/
 * @link https://migelsabre@bitbucket.org/migelsabre/timebarredtoken.git
 */
class TimeBarredTokenFilter extends CFilter {
	/**
	 * @var string Name of the application component
	 */
	public $component = 'timeBarredToken';
	/**
	 * @var string Name of request parameter which contains token string
	 */
	public $tokenName = 'token';
	/**
	 * @var null|integer|float Current time as timestamp. $_SERVER['REQUEST_TIME'] used if NULL.
	 */
	public $time = null;
	/**
	 * @var boolean When token invalid or expired, throws CHttpException when true, return false otherwise.
	 */
	public $throwException = false;
	/**
	 * @var string Custom message for CHttpException
	 */
	public $message = 'Invalid token specified or token expired.';

	/**
	 * Performs token validation.
	 *
	 * @param CFilterChain $filterChain the filter chain that the filter is on.
	 * @return boolean whether the filtering process should continue and the action should be executed.
	 * May also throws CHttpException depending on {@see $throwException} option.
	 * @throws CHttpException
	 */
	protected function preFilter($filterChain) {
		/**
		 * @var TimeBarredTokenComponent $component
		 */
		$component = Yii::app()->getComponent($this->component);
		if($component->validateToken(Yii::app()->request->getParam($this->tokenName), $this->time)) {
			/* token valid an not expired */
			return true;
		}
		/* token invalid or expired */
		if($this->throwException) {
			throw new CHttpException(400, $this->message);
		} else {
			return false;
		}
	}
}
