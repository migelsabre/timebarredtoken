<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2012, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 * @link https://bitbucket.org/migelsabre/timebarredtoken/
 * @link https://migelsabre@bitbucket.org/migelsabre/timebarredtoken.git
 */
class TimeBarredTokenComponent extends CApplicationComponent {
	/**
	 * @var int Token life time in seconds. Default value is 1 day.
	 */
	public $duration = 86400;
	/**
	 * @var null|string Custom encryption key to pass to {@link CSecurityManager::encrypt}
	 */
	public $encryptionKey = null;
	/**
	 * @var null|string Custom validation key to pass to {@link CSecurityManager::validateData}
	 */
	public $validationKey = null;

	/**
	 * Validates token integrity and actuality.
	 *
	 * @param string $token Escaped and encrypted token without any pre-processing.
	 * @param int|float $time Integer or float used as current timestamp, if null value of $_SERVER['REQUEST_TIME'] used.
	 * @return boolean
	 */
	public function validateToken($token, $time = null) {
		if($tokenData = $this->decodeToken($token)) {
			$time = $time ? $time : $_SERVER['REQUEST_TIME'];
			if($tokenData['start'] <= $time && $tokenData['end'] >= $time) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Generates token.
	 *
	 * @param int|null $start Optional, integer or float used as current timestamp, if NULL - value of $_SERVER['REQUEST_TIME'] used.
	 * @param int|null $duration Optional, custom token lifetime in seconds. IF NULL - {@link TimeBarredTokenComponent::$duration} used.
	 * @param null|array $params Optional, additional parameters that will be encoded with token.
	 * @return string Encrypted and escaped token.
	 */
	public function getToken($start = null, $duration = null, $params = null) {
		$data = array_replace_recursive(array(
				'start' => $start ? $start : $_SERVER['REQUEST_TIME'],
				'end' => ($start ? $start : $_SERVER['REQUEST_TIME']) + ($duration ? $duration : $this->duration),
			), $params ? $params : array());
		return $this->escapeToken(\Yii::app()->securityManager->hashData(\Yii::app()->securityManager->encrypt(serialize($data), $this->encryptionKey), $this->validationKey));
	}

	/**
	 * @param string $token
	 * @return bool|array
	 */
	protected function decodeToken($token) {
		$tokenData = \Yii::app()->securityManager->validateData($this->unescapeToken($token), $this->validationKey);
		if(!$tokenData) {
			return false;
		}
		$tokenData = unserialize(\Yii::app()->securityManager->decrypt($tokenData, $this->encryptionKey));
		if(!$tokenData || !is_array($tokenData) || !isset($tokenData['start'], $tokenData['end'])) {
			return false;
		}
		return $tokenData;
	}

	/**
	 * Replace some characters to maximize url compatibility.
	 *
	 * @param string $token Un-escaped token as binary string.
	 * @return string Escaped token.
	 */
	protected function escapeToken($token) {
		return str_replace(array('+', '/', '='), array('_', '-', ''), base64_encode($token));
	}

	/**
	 * Replaces characters, previously replaced by {@link TimeBarredTokenComponent::escapeToken}.
	 *
	 * @param string $token
	 * @return string Un-escaped token as binary string, ready to decoding
	 */
	protected function unescapeToken($token) {
		return base64_decode(str_replace(array('_', '-'), array('+', '/'), $token));
	}
}
